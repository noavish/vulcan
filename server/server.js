const express = require('express');
const http = require('http');
const path = require('path');
const request = require('request');

const app = express();

//Static path to dist
app.use(express.static(path.join(__dirname, '../dist')));

app.get('/api/astronauts', function (req, res) {
  request('http://api.open-notify.org/astros.json', function (error, response, body) {
    const data = JSON.parse(body);
    data.people.forEach(item => {
      item.weight = (Math.random()*200+50).toFixed(2);
    });
    res.json(data.people);
  });
});

// Catch all other routes and return the index file
app.get('*', (req,res)=>{
  res.sendFile(path.join(__dirname,'../dist/index.html'));
});

app.use((err, req, res, next) => {
  res.status(500).send(err);
});

const port = process.env.PORT || '3000';
app.set('port', port);
const server = http.createServer(app);
server.listen(port,()=>console.log(`Server Running on port ${port}`));
