import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AstronautsComponent} from './astronauts/astronauts.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: ''
    },
    component: AstronautsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
