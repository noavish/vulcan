export var adminLteConf = {
  skin: 'blue',
  isSidebarRightToggle: false,
  //isSidebarLeftCollapsed: false,
  //isSidebarLeftExpandOnOver: false,
  //isSidebarLeftMouseOver: false,
  //isSidebarLeftMini: true,
  //sidebarRightSkin: 'dark',
  //isSidebarRightCollapsed: true,
  //isSidebarRightOverContent: true,
  //layout: 'normal',
  sidebarLeftMenu: [
    {label: 'Dashboard', route: '#', iconClasses: 'ion-android-home'},
    {label: 'Exposures', route: '#', iconClasses: 'ion-nuclear'},
    {label: 'Assets', route: '#', iconClasses: 'ion-cash'},
    {label: 'Policy Builder', route: '#', iconClasses: 'ion-pinpoint'},
    {label: 'Connectors', route: '#', iconClasses: 'ion-link'},
    {label: 'Settings', route: '#', iconClasses: 'ion-gear-a'},
    {label: 'Astronauts', route: '/', iconClasses: 'fa fa-space-shuttle'}
  ]
};
