import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Astronaut } from './astronautModel';

@Injectable()
export class AstronautsService {

  constructor( private http: HttpClient ) { }

  getAstronauts(): Observable<Astronaut[]> {
    return this.http.get<Astronaut[]>('/api/astronauts');
  }
}
