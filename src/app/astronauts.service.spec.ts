import { TestBed, inject } from '@angular/core/testing';

import { AstronautsService } from './astronauts.service';

describe('AstronautsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AstronautsService]
    });
  });

  it('should be created', inject([AstronautsService], (service: AstronautsService) => {
    expect(service).toBeTruthy();
  }));
});
