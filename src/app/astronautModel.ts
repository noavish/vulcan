export class Astronaut {
  name: string;
  craft: string;
  weight: number;
}
