import { AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import { AstronautsService } from '../astronauts.service';
import { Astronaut } from '../astronautModel';
import { MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-astronauts',
  templateUrl: './astronauts.component.html',
  styleUrls: ['./astronauts.component.css']
})
export class AstronautsComponent implements OnInit, AfterViewInit {
  displayedColumns = ['name', 'craft', 'weight'];
  dataSource: MatTableDataSource<Astronaut> = new MatTableDataSource<Astronaut>();

  @ViewChild(MatSort) sort: MatSort;
  constructor( private astronautsService: AstronautsService) {}

  ngOnInit() {
    this.astronautsService.getAstronauts().subscribe(
      data => this.dataSource.data = data,
      error => console.log(error)
    );
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
}




